# miniconda3_py37_4.11.0 Singularity container
Based on debian 9.8 slim<br>

Singularity container based on the recipe: Singularity.miniconda3_py37_4.11.0

Package installation using Miniconda3 python3 3.7.4 version miniconda3_py37_4.11.0 <br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>

```bash
singularity pull miniconda3_py37_4.11.0.sif oras://registry.forgemia.inra.fr/gafl/singularity/miniconda3_py37_4.11.0/miniconda3_py37_4.11.0:latest
```

